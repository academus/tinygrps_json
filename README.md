### **API Python para Json:**

Dependências Python:

    Para o desenvolvimento da aplicação foi utilizado uma máquina virtual em python, e algumas dependências foram adicionadas.

    Dependencias Linux:

        sudo apt-get install python3.7-venv
        sudo apt-get install python3.7-dev
        sudo apt install libpq-dev

    Dependencias aplicação py:

        Flask==0.12.2
        wheel

Criando máquina Virtual python:

    python3.7 -m venv venv
    source venv/bin/activate
    python -V # Deve exibir a versão python3.7 no se diretório de desenvolvimento.
    which python #Mostra o caminho do python utilizado, que deve ser dentro de sua pasta de desenvolvimento
    snap install code --classic # caso queiram utilizar o VScode (executar code . dentro do diretório de trabalho da máquina virtual, assim fara uso de tudo que foi parametrizado.)

Rodando aplicação Python:

    python app.py

Arduino IDE:

    Versão 1.8.1

TinyGPRS:

    https://github.com/vshymanskyy/TinyGSM