#define TINY_GSM_MODEM_SIM800 // definição do modem usado (SIM800L)
#include <TinyGsmClient.h> // biblioteca com comandos GSM

// objeto de comunicação serial do SIM800L
HardwareSerial SerialGSM(1);
 
// objeto da bibliteca com as funções GSM
TinyGsm modemGSM(SerialGSM);

/*#ifdef USE_SSL
  TinyGsmClientSecure client(modemGSM);
  const int  port = 443;
#else*/
  TinyGsmClient client(modemGSM);
  const int  port = 8080;
//#endif
 

//Define configuracao do servidor
#define server "ip do servidor"
#define port 8080
const char funcionarioAdress[] = "/funcionario";

//Access point name da vivo
const char *APN = "tim.br";
//Usuario, se não existir deixe em vazio
const char *USER = "tim";
//Password, se não existir deixe em vazio
const char *PASSWORD = "tim";

//Definicoes do rep
#define repID 202000001
#define empresa "empresa0001"

String dataM="03/03/2020";
String hora="19:54";
String userID="2BA5F000";

bool printLcd=true;

void setup() {
 
  Serial.begin(115200);
  Serial.println("Starting...");
  setupGSM();
  

}

void loop() {


  // put your main code here, to run repeatedly:

  String imei = modemGSM.getIMEI();
  
  Serial.println(imei);
  sendJson(createJsonFuncionario(userID, dataM+" "+hora), funcionarioAdress);
  delay(10000);

}


void sendJson(String json, String resource)
{
  String imei = modemGSM.getIMEI();

  Serial.print("Connecting to ");
  Serial.println(String(server)+":"+String(port));
  if (!client.connect(server, port)) {
    Serial.println(" fail");
    delay(10000);
    return;
  }
  Serial.println(" OK");

  //Make a Post json Data
  Serial.println("Performing Json Post request...");
  String httpRequestData = json;
  
  client.print(String("POST ") + resource + " HTTP/1.1\r\n");
  client.print(String("Host: ") + server + "\r\n");
  client.println("Connection: close");
  client.println("Content-Type: application/json");
  client.print("Content-Length: ");
  client.println(httpRequestData.length());
  client.println();
  client.println(httpRequestData);
  Serial.println(httpRequestData);


  unsigned long timeout = millis();
  while (client.connected() && millis() - timeout < 10000L) {
    // Print available data
    while (client.available()) {
      char c = client.read();
      Serial.print(c);
      timeout = millis();
    }
  }
  Serial.println();

  delay(300);
  client.stop();
  
}


void setupGSM()
{
  Serial.println("Setup GSM...");
  //Inicializamos a serial onde está o modem
  SerialGSM.begin(115200, SERIAL_8N1, 4, 2, false);
  delay(3000);
 
  //Mostra informação sobre o modem
  Serial.println(modemGSM.getModemInfo());
 
  //Inicializa o modem
  if (!modemGSM.restart())
  {
    Serial.println("Restarting GSM Modem failed");
    delay(10000);
    ESP.restart();
    return;
  }
 
  //Espera pela rede
  if (!modemGSM.waitForNetwork()) 
  {
    Serial.println("Failed to connect to network");
    delay(10000);
    ESP.restart();
    return;
  }
 
  //Conecta à rede gprs (APN, usuário, senha)
  if (!modemGSM.gprsConnect("", "", "")) {
    Serial.println("GPRS Connection Failed");
    delay(10000);
    ESP.restart();
    return;
  }
 
  Serial.println("Setup GSM Success");
}


String createJsonFuncionario(String userID, String Data) 
{  
  String data = "{";
    data+="\"repID\":";
    data+="\""+String(repID)+"\"";
    data+=",";
    data+="\"data\":";
    data+="\""+Data+"\"";
    data+=",";
    data+="\"userID\":";
    data+="\""+userID+"\"";
    data+="}";
  return data;
}
