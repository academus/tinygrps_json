from flask import Flask, request, jsonify, json
from time import sleep
from datetime import datetime
import os

app = Flask(__name__)


@app.route('/srep/1.0/funcionario', methods=['POST'])
def funcionario():
    if request.method == 'POST':
        print (request.is_json)
        data = request.get_json()
        #print(data)
        condicao = save_funcionario(data)
        if condicao == True:
            response=jsonify(
                status=200
            )
            return response
        else:
            response=jsonify(
                status=500
            )
            return response
    return html


@app.route('/postjson', methods = ['POST'])
def postJsonHandler():
    print (request.is_json)
    content = request.get_json()
    print (content)
    return 'JSON posted'

def save_funcionario(funcionario):
    repID = funcionario['repID']
    data_e_hora = datetime.strptime(funcionario['data'], '%d/%m/%Y %H:%M')
    userID = funcionario['userID']
    if repID != "" and data_e_hora !="" and userID !="":
        return True
    else:
        return False
    
if __name__ == "__main__":
    app.run(use_reloader=True, debug=True, host="0.0.0.0", port=8080)



